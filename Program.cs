﻿using System;
using System.Collections.Generic;

namespace BoxBlur
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
        static int[][] boxBlur(int[][] image) {
            List<int[]> newImage = new List<int[]>();
            
            for(int row = 0; row < image.Length - 2; row++){
                List<int> line = new List<int>();
                for(int col = 0; col < image[row].Length - 2; col++){
                    int sum = 0;
                    for(int k = row; k < row + 3; k++){
                        for(int l = col; l < col + 3; l++){
                            sum += image[k][l];
                        }
                    }
                    line.Add(sum /9);
                }
                newImage.Add(line.ToArray());
                
            }
            return newImage.ToArray();

        }

    }
}
